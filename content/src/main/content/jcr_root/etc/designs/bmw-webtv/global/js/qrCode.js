(function(){

    //二维码提示框对象
    var qrCode = function(arg) {
        return new qrCode.fn.initialize(arg);
    };

    //参数对象构建不同的二维码提示框对象
    var defaultOpt = {
        tar:'',
        title:'',
        content_title:'',
        content:'',
        url:'',
        width:162,
        height:162
    };

    qrCode.fn = qrCode.prototype = {
        initialize: function(arg) {
            $.extend(this, defaultOpt, arg);
            //this.bindMouseEnd();

            var  thisQr = this;
            this.tar.on("tap", function() {
                
                $('body').append(thisQr.temp(thisQr));

                if(thisQr.url){
                    $('#qrcode').qrcode({  width:thisQr.width,height:thisQr.height,text:thisQr.url});
                };
                

                var top = ($(window).height() - $('.qrcode-box').innerHeight())/2; 
                var scrollTop = $(document).scrollTop(); 

                $('.qrcode-box').css( { position : 'absolute', 'top' : top + scrollTop} );
            });

            $("body").delegate(".close-qrcode-box","tap",function(e){
                 $('.qrcode-box').remove();
                 $('.qrcode-box-bg').remove();
                 e.stopPropagation();
            });
        },
        temp:function(parm){
            return '<div class= "qrcode-box" style="display:block;">'  
                        +'<h3>'+parm.title+'<div class="close-qrcode-box">×</div></h3>'
                        +'<div class="video-title"><h4>'+parm.content_title+'</h4></div><div id="qrcode"  class="f-content">'+parm.content+'</div>'
                        +'<p>使用微信扫一扫</p>'
                    +'</div>'
                    +'<div class="qrcode-box-bg"><div>';
        }

    };

    qrCode.fn.init = function() {
        
    };

    qrCode.fn.initialize.prototype = qrCode.fn;
    window.qrCode = qrCode ;

})();