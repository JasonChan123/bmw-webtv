/*!
 *
 *  BMW Starter Kit
 *  Copyright 2016 ACN Inc. All rights reserved.
 *  
 *  @Author：ACN team @Date: 2016-06-18
 *  
 */
'use strict';

(function(){
    var apiRoot = null;
})();

//主页面视频模块
$(document).ready(function() {
    apiRoot =  $('#apiRoot').text();       
    $('#myinp').bind('keyup', function(event) {
                    if (event.keyCode == "13") {
                    $('#seachButton').click();
                }
                });

   (function(){
     $(".ulist").each(function(index,e3){
                          var classInfo = $(e3).attr("id");
                          var classes = classInfo.split(",");
         loadVideos(e3,classes[0],classes[1],classes[2],classes[3],classes[4],classes[5],classes[6],index);

         });
    })();


 	(function(){
     $(".ulists").each(function(index3,e3){
        var classInfo = $(e3).attr("id");
        var classcc = classInfo.split(",");

        loadfirstVideos(e3,classcc[0],classcc[1],classcc[2],classcc[3],classcc[4],classcc[5],index3);

       });
    })();

	(function(){
	  // 搜索框
    $('.navbar .searchButton').click(function() {
        $(".searchforresult").slideToggle(300);
    });

    $('.searchforresult .seachClose').click(function() {
        $(".searchforresult").slideUp(200);
    });
	})();

});
			 
//加载视频
function loadVideos(e2,s2,s3,s4,s5,s6,s7,path,index) {
                path = rewriteUrl(path);
                var s8 = s5*s7;

              $.ajax({
                  url: apiRoot+'/getVideosbyC',
                  data:{class1:s2,class2:s3,class3:s4,orderBy:"newest",currentPage:1,count:s8},
                  type:"get",
                  dataType:"jsonp",
                  jsonp: "callbackparam",
                  jsonpCallback: "CALLBACK"+index,
                  success:function(response){
                      data = response.responseBody;
                      var video = data["videos"];
                      if(video.length>0){
                          var car = video[0]["class2"];
                          $(".mycartest").each(function(index3,e3){
                              if($(e3).hasClass(s6)){
                                  $(e3).append(car);
                              }

                          });
                          var items = [];
                          for(var i in video){
                              var  videodata = video[i]; 
                              items[0] = videodata["vid"];
                              items[1] = path;
                              var title = videodata["title"];
                              title = title.replace(/(_)|(-)/g,' ');
                              $(e2).append("<li><span class='sap'  onclick='play(\""+items+"\")'><span class='picandsha'><img src='"+videodata["bigThumbnail"]+"' alt='' ><span class='shadow-intro'><img src='/etc/designs/bmw-webtv/global/images/bmwTv/tra.png' alt='' ></span></span><a href='#'  >"+title+"</a><p class='eye-r4' ><i class='icon-11icon2-01'></i>"+videodata["video_view"]+"</p></span></li>");
                              if(i==video.length-1)break;

                          }
                          
                      }

                 }
            });
      };

//加载视频
function loadfirstVideos(e2,s2,s3,s4,s5,s6,path,index) {
              path = rewriteUrl(path);
              $.ajax({
                  url:apiRoot+'/getVideosbyC',
                  type:"get",
                  dataType:"jsonp",
                  jsonp: "callbackparam",
                  jsonpCallback: "CALLBACKFirst"+index,
                  data:{class1:s2,class2:s3,class3:s4,orderBy:"newest",currentPage:1,count:4},
                  success:function(response){
                      var data = response.responseBody;
                      var video = data["videos"];
                      var car = video[0]["class2"]

                      $(".mycartest").each(function(index3,e3){
                          if($(e3).hasClass(s6)){
                              $(e3).append(car);
                          }

                      });
                      var items = [];
                      for(var i in video){
                          var  videodata = video[i]; 
                          items[0] = videodata["vid"];
                          items[1] = path;
                          var title = videodata["title"];
                          title = title.replace(/(_)|(-)/g,' ');
                          $(e2).append("<li><span class='sap'   onclick='play(\""+items+"\")'><span class='picandsha'><img src='"+videodata["bigThumbnail"]+"' alt='' ><span class='shadow-r4'><img src='/etc/designs/bmw-webtv/global/images/bmwTv/tra.png' alt='' ></span></span><a href='#'  >"+title+"</a><p class='eye-r4' ><i class='icon-11icon2-01'></i>"+videodata["video_view"]+"</p></span></li>");
                          if(i==video.length-1)break;

                      }
                 }
            });
};

//播放
function play(item){
      var items =  item.split(","); 
      var id = items[0];
      window.location.href=""+items[1]+".video_id="+id+".html";
}
//搜索
function getNum(text){
    var value = text.replace(/[^0-9]/ig,""); 
    return value;
}

function submit(path){

  var value = $("#myinp").val();
  var num = getNum(value);
  var keywords = value;
   window.location.href=""+path+".html?keywords="+keywords;
}
//获取URL参数字段
function GetQueryString(name)
{
  var reg = new RegExp("(^|.)"+ name +"=([^.]*)(.|$)");
  var r = decodeURI(window.location.pathname).match(reg);
    if(r!=null) {
    return r[2];
    }
  return null;
} 

// rewrite url accoring to current environment
function rewriteUrl(path){
    var pos = location.pathname.indexOf($("#shortenBase").val());
    var mapper = $("#shortenMapper").val();

    if(pos == -1){//need to rewrite.
        var mapperArr = mapper.split(';');
        for(var i=0; i<mapperArr.length; i++){
            var repArr = mapperArr[i].split(':');
            if(repArr.length == 2){
                path = path.replace(repArr[0], repArr[1]);
            }
        }
    }
    return path;
}
