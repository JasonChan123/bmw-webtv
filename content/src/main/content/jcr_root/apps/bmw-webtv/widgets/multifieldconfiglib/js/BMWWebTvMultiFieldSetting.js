

CQ.Ext.ns("BMWWEBTV.multifieldsetting");

BMWWEBTV.multifieldsetting.BmwWebTvMultiFieldSetting = CQ.Ext.extend(CQ.form.CompositeField, {
	/**
	 * @private
	 * @type CQ.Ext.form.Hidden
	 */
	hiddenField: null,

	/**
	 *  @private
	 *  Array holding child items
	 */
	myObjects: null,

	/**
	 * @private
	 * @type String
	 */
	fieldSeparator: "|",

    multiConfig: null,

	constructor: function(config) {
		config = config || {};
		var defaults = {
			"border": false,
			"layout": "form",
			"columns":2
		};
		if(config.separator) {
			this.fieldSeparator = config.separator;
		}

		config = CQ.Util.applyDefaults(config, defaults);
        this.multiConfig = config;
		BMWWEBTV.multifieldsetting.BmwWebTvMultiFieldSetting.superclass.constructor.call(this, config);
	},

	// overriding CQ.Ext.Component#initComponent
	initComponent: function() {
		BMWWEBTV.multifieldsetting.BmwWebTvMultiFieldSetting.superclass.initComponent.call(this);
		this.hiddenField = new CQ.Ext.form.Hidden({
			name: this.name
		});
		this.add(this.hiddenField);

		this.myObjects = [];

		for(var i = 0; i < this.customFields.length; i++) {
			this.myObjects[i] = CQ.Util.build(this.customFields[i]);
            //fix issue: for checkbox type, if use build to initialize the field, change event can't be changed, and 
            if(this.myObjects[i].xtype == "selection" && this.myObjects[i].type == "checkbox"){ 
                this.myObjects[i] = new CQ.Ext.form.Checkbox({
                    fieldLabel: this.myObjects[i].fieldLabel,
                    cls: this.myObjects[i].cls
                });
            }

            if(this.multiConfig && this.multiConfig.layout == "table"){
				this.add(new CQ.Ext.form.Label({cls: this.multiConfig.commonLabelCls,text: this.myObjects[i].fieldLabel}));
            }
            if(this.multiConfig && this.multiConfig.layout == "form"){
				this.myObjects[i].labelStyle = this.multiConfig.commonLabelStyle +　this.myObjects[i].labelStyle;
            }

			this.add(this.myObjects[i]);
			this.myObjects[i].on("change", this.updateHidden, this);
			// needed for pathfields
			this.myObjects[i].on("dialogselect", this.updateHidden, this);
			// for select boxes
			if (this.myObjects[i].type == "select"){
				this.myObjects[i].on("selectionchanged", this.updateHidden, this);
			}
		}
	},

	// overriding CQ.form.CompositeField#setValue
	setValue: function(value) {
		if(value) {
			var parts = value.split(this.fieldSeparator);
			for(var i = 0; i < this.myObjects.length; i++) {
                var objValue = parts[i];
                if(this.myObjects[i].xtype == "pathfield" && objValue != null && objValue.indexOf(".") == -1 
                		&& objValue.indexOf("/content") == 0 && objValue.indexOf("/content/dam") == -1
                        && (!this.myObjects[i].noSuffix || this.myObjects[i].noSuffix == false)) {
					objValue += ".html";
                }
				this.myObjects[i].setValue(objValue);
			}

			this.updateHidden();
		}
	},

	// overriding CQ.form.CompositeField#getValue
	getValue: function() {
		return this.getRawValue();
	},

	// overriding CQ.form.CompositeField#getRawValue
	getRawValue: function() {
		if(!this.myObjects) {
			return null;
		}
		var value = "";

		for(var i = 0; i < this.myObjects.length; i++) {
            var objValue = this.myObjects[i].getValue();

            if(this.myObjects[i].xtype == "pathfield" && objValue != null && objValue.indexOf(".") == -1 
                		&& objValue.indexOf("/content") == 0 && objValue.indexOf("/content/dam") == -1
                        && (!this.myObjects[i].noSuffix || this.myObjects[i].noSuffix == false)) {
				objValue += ".html";
            }

			value += objValue;
			if(i < this.myObjects.length - 1) {
				value += this.fieldSeparator;
			}
		}

		return value;
	},


	// private
	updateHidden: function() {
		this.hiddenField.setValue(this.getValue());
	}
});

// register xtype
CQ.Ext.reg('bmwwebtvmultifieldsetting', BMWWEBTV.multifieldsetting.BmwWebTvMultiFieldSetting);
