$(document).ready(function(){
    // categories data must be load firstly.
    $.ajax({
            url: $('#apiRoot').text()+'/getVideosCategories?t=1',
            type:"get",
            dataType:"jsonp",
            jsonp: "callbackparam",
            jsonpCallback: "CALLBACKWidget",
            success:function(data){
                var allCategories = data.responseBody;
                
                // create VideoWidget
                VideoItem.VideoWidget = CQ.Ext.extend(CQ.form.CompositeField, {
                    /**
                     * @private
                     * @type CQ.Ext.form.TextField
                     */
                    hiddenField: null,

                     /**
                     * @private
                     * @type CQ.Ext.form.ComboBox
                     */
                    videoOne: null,
                     /**
                     * @private
                     * @type CQ.Ext.form.ComboBox
                     */
                    videoTwo: null,
                     /**
                     * @private
                     * @type CQ.Ext.form.ComboBox
                     */
                    videoThree: null,

                    /**
                     * @private
                     * @type CQ.Ext.form.TextField
                     */

                    formPanel: null,
                     handData : null,
                    constructor: function(config) {
                        config = config || { };
                        var defaults = {
                            "border": true,
                            "layout": "table",
                            "columns": 3
                        };
                        config = CQ.Util.applyDefaults(config, defaults);
                        VideoItem.VideoWidget.superclass.constructor.call(this, config);
                    },

                    // overriding CQ.Ext.Component#initComponent
                    initComponent: function() {
                        VideoItem.VideoWidget.superclass.initComponent.call(this);
                        //Hidden Field
                        this.hiddenField = new CQ.Ext.form.Hidden({
                            name: "./sectionTitle"
                        });

                        this.add(this.hiddenField);

                        this.handData = allCategories;

                        //DROP DOWN
                        this.videoOne = new CQ.form.Selection({
                            type:"select",
                            name:"./videoone",
                            width:100,
                            defaultValue: "请选择",
                            value:"请选择"
                        });
                        this.add(new CQ.Ext.form.Label({
                            text: "Level1",
                            })); 
                        this.add(this.videoOne);

                        this.videoTwo = new CQ.form.Selection({
                            type:"select",
                            name:"./videotwo",
                            width:200,
                            defaultValue: "请选择",
                            value:"请选择"
                        });
                        this.add(new CQ.Ext.form.Label({
                            text: "Level2",
                           }));
                        this.add(this.videoTwo);

                        this.videoThree = new CQ.form.Selection({
                            type:"select",
                            name:"./videothree",
                            width:100,
                            defaultValue: "请选择",
                            value:"请选择"
                        });
                        this.add(new CQ.Ext.form.Label({
                            text: "Level3",
                            }));
                        this.add(this.videoThree);

                    },


                      linkToDependentField:function (a) {

                            var self = this;
                            this.videoOne.setOptions(this.oneclassInfo());
                          
                            var one = a["videoone"];
                            var two = a["videotwo"];
                            var three = a["videothree"];
                            
                          if(one == null || one=="请选择"){
                              //nothing is selected
                             this.videoOne.on('selectionchanged', function (selection, value) {
                                 self.hiddenField.setValue(self.getCategoryNameById(value));
                                 self.dependentOnValueChanged1(value);
                              });

                          }else{
                            // selected values exist
                            this.videoOne.setValue(one);
                            this.videoTwo.setOptions(this.twoclassInfo(one));
                            this.videoTwo.setValue(two);
                            this.videoThree.setOptions(this.threeclassInfo(one,two));
                            if(!two){
                                this.videoTwo.setValue("请选择");
                            }
                            if(!three){
                                this.videoThree.setValue("请选择");
                            }  
                              
                            this.videoOne.on('selectionchanged', function (selection, value) {
                                one = value;
                                self.hiddenField.setValue(self.getCategoryNameById(one));
                                self.dependentOnValueChanged2(value);
                            });
                              
                            this.videoTwo.on('selectionchanged', function (selection, value) {
                                if(value=="请选择"){
                                    self.hiddenField.setValue(self.getCategoryNameById(one));
                                }else{
                                    self.hiddenField.setValue(self.getCategoryNameById(one,value));
                                }
                                self.newDependentOnValueChanged2(one,value);
                            });
                          }

                      },
                    
                     dependentOnValueChanged1: function (one) {
                        var self = this;
                        this.videoTwo.reset();
                        this.videoTwo.setOptions(this.twoclassInfo(one));
                        this.videoThree.reset();
                        this.videoTwo.on('selectionchanged', function (selection, newTwoValue) {
                             self.hiddenField.setValue(self.getCategoryNameById(one,newTwoValue));
                             self.videoThree.setOptions(self.threeclassInfo(one,newTwoValue));
                        });
                     },
                    
                     dependentOnValueChanged2: function (one) {
                        var self = this;
                         this.videoTwo.reset();
                        this.videoThree.reset();
                        
                         // if class1 change, reset value for class2 & class3
                        this.videoTwo.setOptions(this.twoclassInfo(one));                        
                        this.videoThree.setOptions([{text:"请选择",value:"请选择"}]);
                                                   
                        this.videoTwo.on('selectionchanged', function (selection, newTwoValue) {
                            self.hiddenField.setValue(self.getCategoryNameById(one,newTwoValue));
                            self.newDependentOnValueChanged2(one,newTwoValue);
                        });
                     },

                     newDependentOnValueChanged2: function(one,two){
                        this.videoThree.reset();
                        this.videoThree.setOptions(this.threeclassInfo(one,two));
                    },
                     oneclassInfo:function(){
                        var info = [{
                            text:"请选择",
                            value: "请选择"
                        }];
                        var currentValues = this.handData;
                        for(var i in currentValues){
                            if(i == "remove") break;
                                info.push({
                                    text:currentValues[i].name,
                                    value:currentValues[i].id
                                });
                          }
                         return info;
                     },
                      twoclassInfo:function(selectedValue){
                        var info = [{
                                    text:"请选择",
                                    value: "请选择"
                                    }];
                          var secondClass = null;
                          for(var i=0;i<this.handData.length;i++){
                              var firstClass = this.handData[i];
                              if(firstClass["id"]==selectedValue){
                                  secondClass = firstClass["children"];
                                  break;
                              }
                          }
                            for(var i in secondClass){
                                if(i == "remove") break;
                                info.push({
                                    text:secondClass[i].name,
                                    value:secondClass[i].id
                                });
                            }

                            return  info;
                    },
                    threeclassInfo:function(one,two){

                        var info = [{
                            text:"请选择",
                            value: "请选择"
                        }];
                        if(two && two!="请选择"){
                            var selectedFirstClass = null;
                            for(var i=0;i<this.handData.length;i++){
                                var firstClass = this.handData[i];
                                if(firstClass["id"] == one){
                                    selectedFirstClass = firstClass;
                                    break;
                                }
                            } 

                            var secondClassAry = selectedFirstClass["children"];
                            
                            var selectedSecondClass = null;
                            for(var i=0;i<secondClassAry.length;i++){
                                var secondClass =secondClassAry[i];
                                if(secondClass["id"] == two){
                                    selectedSecondClass = secondClass;
                                    break;
                                }
                            }

                            var thirdClassAry = selectedSecondClass['children'];

                            if(thirdClassAry.length!=0){
                                 for(var i in thirdClassAry){

                                    if(i == "remove") break;
                                    info.push({
                                        text:thirdClassAry[i].name,
                                        value:thirdClassAry[i].id
                                    });
                                }
                            }
                        }

                        return  info; 
                    },
                    // overriding CQ.form.CompositeField#processPath
                    processPath: function(path) {
                        this.videoOne.processPath(path);
                        this.videoTwo.processPath(path);
                        this.videoThree.processPath(path);
                        var a = this.newUrl(path);
                        this.linkToDependentField(a);

                    },

                    // overriding CQ.form.CompositeField#processRecord
                    processRecord: function(record, path) {
                        this.videoOne.processRecord(record, path);
                        this.videoTwo.processRecord(record, path);
                        this.videoThree.processRecord(record, path);
                    },
                    
                    // private
                    updateHidden: function() {
                        this.hiddenField.setValue(this.getValue());
                        var typeVal = this.videoType.getValue();
                        this.switchVideoPath(typeVal);
                    },


                     newUrl: function(url){
                        var info = [];
                        $.ajax({
                            url:''+url+'.infinity.json',
                            type:'get',
                            dataType:'json',
                            async : false, 
                            success:function(data){
                                info = data;
                            }
                        }); 

                             return  info;
                     },
                    getCategoryNameById : function(one,two){
                        var name = null;
                        for(var i=0; i< allCategories.length;i++){
                            var obj =  allCategories[i];
                            if(obj.id == one){
                                if(!two){
                                    name = obj.name;
                                    return name;
                                }else{
                                    for(var j=0;j<obj.children.length;j++){
                                        if(obj.children[j].id==two){
                                            name = obj.children[j].name;
                                            return name;
                                        }
                                    }
                                }
                            }
                        }
                        return name;
                    }
            });

            // register xtype
            CQ.Ext.reg("videoitems", VideoItem.VideoWidget);

        }
    });

});