/*
 * Copyright (c) 2014 Adobe Systems Incorporated. All rights reserved.
 *  
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Please note that some portions of this project are written by third parties
 * under different license terms. Your use of those portions are governed by
 * the license terms contained in the corresponding files. 
 */

/**
 * Returns an object with following member:
 * {String} updatePath: The URL on which the web app can retreive an HTML fragment to update the view
 */
use(function(){

        var info = [];
		var classOne =  properties.get("videoone");
    	var classTwo =  properties.get("videotwo");
   		var classThree =  properties.get("videothree");
    	var styleType = properties.get("videoitems");
    	var videoRows = properties.get("row");
    	var imagePath = properties.get("imagePath");
        var sectionTitle = properties.get("sectionTitle");

       var myjson = currentDesign.getJSON();
    	var jsonObj = JSON.parse(myjson);
		  var detailPath = "null";
    	var tabsviewPath = "null";
    	if(jsonObj.indexPage && jsonObj.indexPage.headerPar && jsonObj.indexPage.headerPar.headerarea && jsonObj.indexPage.headerPar.headerarea.detailPath ){
			  detailPath = jsonObj.indexPage.headerPar.headerarea.detailPath;
    	}
    	if(jsonObj.indexPage && jsonObj.indexPage.headerPar && jsonObj.indexPage.headerPar.headerarea && jsonObj.indexPage.headerPar.headerarea.tabsviewPath){
           tabsviewPath =jsonObj.indexPage.headerPar.headerarea.tabsviewPath ;
    	}

   /* try {
    	var detailPath = pathjson.indexPage.headerPar.headerarea.detailPath ;
     	var tabsviewPath =pathjson.indexPage.headerPar.headerarea.tabsviewPath ;
    } catch (err) {
		// please first config detail & tabsview page path
    }*/

    var a =  properties.path;
    var b = "";
    if(a == null){
		b="flag";
    }else{
     b = a.substring(a.lastIndexOf('/')+1); }

	if(classOne &&!classOne.endsWith('x')){
        classOne = null;
    }
    if(classTwo && !classTwo.endsWith('x')){
        classTwo=null;
    }

    if(classThree && !classThree.endsWith('x')){
		classThree=null;
    }
    
    if(styleType == ""){
		styleType=2;
    }
    if(videoRows == null){
		videoRows=2;
    }
    
    var adimgpath = null;
    if(imagePath !=null && imagePath != "")
		//adimgpath = imagePath.replace('/content/','/');
    	adimgpath =  imagePath;
	 info = [classOne,classTwo,classThree,styleType,b,videoRows,imagePath,detailPath,tabsviewPath,adimgpath,sectionTitle];

    return  info;

})
