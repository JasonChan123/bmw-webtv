package apps.bmw_webtv.parComponent.headerarea;

import com.adobe.cq.sightly.WCMUse;
import com.bmw.digitalcn.aems2.service.ConfigurationService;

public class WebTVApiComponent extends WCMUse{
    private String webtvApiUrl = null;

	@Override
    public void activate() throws Exception {
		ConfigurationService configurationService = getSlingScriptHelper().getService(com.bmw.digitalcn.aems2.service.ConfigurationService.class);
		webtvApiUrl = configurationService.getWebtvApiUrl();
    }

    public String getWebtvApiUrl() {
		return webtvApiUrl;
	}
}