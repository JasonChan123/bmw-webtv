package apps.bmw_webtv.parComponent.footerarea;
  
import com.adobe.cq.sightly.WCMUse;
import javax.jcr.Node;

public class Foot extends WCMUse {
    private String path = null;
  
    @Override
    public void activate() throws Exception {
        Node node = getResource().adaptTo(Node.class);
        if(node.hasNode("qr1") && node.getNode("qr1").hasProperty("fileReference")) {
			path = node.getNode("qr1").getProperty("fileReference").getString();
    	} else {
        	path = getResource().getPath() + "/qr1/file/jcr:content";
    	}
        path = path.replaceAll("\\","/");
        System.out.println("path="+path);
    }

    public String getPath() {
        return path;
    }
}