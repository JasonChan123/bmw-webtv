package apps.bmw_webtv.parComponent.tabsview;
  
import com.adobe.cq.sightly.WCMUse;
import javax.jcr.Node;
  
public class Tabsview extends WCMUse {
    private String title = null;
  
    @Override
    public void activate() throws Exception {
        Node node = getResource().adaptTo(Node.class);
        if(node.hasNode("tabsviewimage") && node.getNode("tabsviewimage").hasProperty("jcr:title")) {
			title = node.getNode("tabsviewimage").getProperty("jcr:title").getString();
    	}
    }

    public String getTitle() {
        return title;
    }
}