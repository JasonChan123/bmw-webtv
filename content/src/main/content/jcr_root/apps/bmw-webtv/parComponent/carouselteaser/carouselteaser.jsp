<%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" contentType="text/html; charset=utf-8" %><%
%>
<%@page import="java.lang.String"%>

<div class="banner" style="">
        <div id="demo-slide"></div>
</div>
<%
    Object settings = (Object)properties.get("settings",null);
	String[] values = null; 
    if(settings != null){
        if(settings instanceof String){
            values = new String[]{(String)settings};
        }else{
            values = (String[]) settings;
        }
    }
	if(values != null && values.length > 0) {
        out.println("<script type=\"text/javascript\"> ");
        out.println(" $(document).ready(function() {");
        //out.println(" (function() {");
        out.println(" new BMWTvGrid(");
        out.println(" 'demo-slide', {}, [");
        int valuesLen = values.length;
		for(int i=0; i<valuesLen; i++) {
            String[] rowValues = values[i].split(";");
			//rowValues[3] = rowValues[3].replaceAll("_"," ");
            //out.println("{\"img\": \"/etc/designs/bmw-webtv/clientlib/images/bmwTv/"+(i+1)+".jpg\",");
            //out.println("\"title\": \"宝马GT 车窗\",");
            //out.println("\"videoUrl\": \"http://player.youku.com/embed/XMTUwNzM4ODMzMg==\",");
            String imgPath = rowValues[0];
            if(imgPath.startsWith("/content/"))
                //imgPath = imgPath.replaceFirst("/content", "");
            out.println("{\"img\": \""+ imgPath +"\",");
            out.println("\"videoUrl\": \""+ rowValues[1] +"\",");
            out.println("\"yk_id\": \""+ rowValues[2] +"\",");
            out.println("\"title\": \""+ rowValues[3] +"\"}");
            if(i < valuesLen-1) {
				out.println(",");
            }
    	}
        out.println("]");
        out.println(");");
        //out.println("// 2*2+1等高度");
        //out.println("$('.OW-trainng').each(function() {");
        //out.println("$(this).children('.card-item').matchHeight({");
        //out.println("byRow: true");
        //out.println("});");
        //out.println("});");
        out.println("});");
        out.println("</script>");
	} else {
        out.println("<h4>Please set carousel teaser</h4>");

	}
%>